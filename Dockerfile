ARG BASE_IMAGE=uprev/base
ARG BASE_TAG=ubuntu-18.04
FROM ${BASE_IMAGE}:${BASE_TAG}


ARG CCSTUDIO_INSTALLER_URL=https://dr-download.ti.com/software-development/ide-configuration-compiler-or-debugger/MD-J1VdearkvK/12.2.0/CCS12.2.0.00009_linux-x64.tar.gz
ARG ENABLE_COMPONENTS=PF_MSP430,PF_CC3X 
RUN dpkg --add-architecture i386

RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \  
    software-properties-common      \
    ca-certificates                 \
    curl                            \
    libgconf-2-4                    \
    libdbus-glib-1-2                \
    libxtst6                        \
    libc6:i386                      \
    libncurses5:i386                \
    libstdc++6:i386                 \
    lib32z1                         \
    libbz2-1.0:i386                 \
    libx11-6:i386                   \
    libxext6:i386                   \
    libxrender1:i386                \
    libxtst6:i386                   \
    libxi6:i386                     \
    at-spi2-core                    \
    binutils                        \
    && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 

RUN mkdir /ccs_install
RUN curl -L ${CCSTUDIO_INSTALLER_URL} | tar xvz --strip-components=1 -C /ccs_install 
RUN /ccs_install/ccs_setup_*.run --mode unattended --prefix /opt/ti --enable-components ${ENABLE_COMPONENTS}
RUN rm -rf /ccs_install

ENV ECLIPSEC=/opt/ti/ccs/eclipse/eclipse
