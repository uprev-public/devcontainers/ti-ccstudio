TI Code Composer Studio 
=======================

This repo contains the Dockerfile and devcontainer configuration for ``uprev/ti-ccstudio```. It has all of the dependencies for building a Code Composer Studio project

There is a devcontainers.json file, so you can add this repo as a submodule to your project to use as a devcontainer:

.. code::bash 

    git submodule add https://gitlab.com/up-rev-public/devcontainers/base.git .devcontainer


Build Args 
----------

Variables are used so that multiple tags can be set up in CI/CD

- BASE_IMAGE: base image to build from 
- BASE_TAG: Tag of base image to use 
- CCSTUDIO_INSTALLER_URL: url of CCStudio installer. 
